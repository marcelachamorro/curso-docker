# Unidad 2 - Proyecto Backend  Spring Boot Mongo

# 1  BackEnd Spring Boot

```
./gradlew build
```
El primer problema es que la JAVA necesaria para compilar el proyecto mediante gradlew (Java + gradle Wapper)
no esta instalada. (Supongamos, es posible que en su caso la tenga instalada previamente)

## 1.1 Entorno de Desarrollo para Java con Docker

Podemos ejecutar el siguiente comando para vincular un contenedor Java11 con nuestro proyecto
Esto permite generar el proyecto sin la necesidad de tener Java en la Maquina de desarrollo

```
docker run --rm -it --name java-docker \
-v "$PWD":/usr/app/ -w /usr/app/ \
-p 8080:8080  \
openjdk:11-slim /bin/bash
```

### 1.1.1  Crear una Imagen y un Compose de Desarrollo

Con el siguiente **Dockerfile** creamos una imagen
```
FROM openjdk:11-slim
WORKDIR /usr/app/
EXPOSE 8080
ENTRYPOINT /bin/bash
```

Con el siguiente commando creamos la imagen para desarrollo **java-docker**
```
docker build -t java-docker . -f DockerfileDev

docker run --rm -it --name java-docker \
-v "$PWD":/usr/app/ -w /usr/app/ \
-p 8080:8080  \
java-docker
```

**docker-compose** con el servicio de base de datos necesario para que pueda levantar el servicio
```
version: "3.4"
services:
  mongodb_dev:
    image: mongo:4.2.6
  java_dev_env:
    build:
      context: .
      dockerfile: DockerfileDev
    ports:
      - 8080:8080
    volumes:
      - ./:/usr/app
    depends_on:
      - mongodb_dev
```

```
docker-compose -f docker-compose-dev.yml  run --rm -p 8080:8080 java_dev_env
```
  
Con el siguiente comando podemos obtener el **ID del contenedor**

```
docker ps -qf "name=backend_java_dev_env" 
```

Ahora ya podemos ingresar al contenedor Java11 con bash o directamente ejecutar las tareas de gradlew (build/clear/bootrun)
```
docker exec -it $(docker ps -qf "name=backend_java_dev_env") /bin/bash
docker exec -it $(docker ps -qf "name=backend_java_dev_env") ./gradlew bootrun
docker exec -it $(docker ps -qf "name=backend_java_dev_env") ./gradlew clean
docker exec -it $(docker ps -qf "name=backend_java_dev_env") ./gradlew build
```

# 1.2 Base de datos mongo

## 1.2.1 Imagen Repo oficial
[Docker Hub Imagen Mongo Oficial link](https://hub.docker.com/_/mongo) 

```
docker pull mongo:4.2.6
```

## 1.2.2 Crear contenedor en el puerto 27017
```
docker run -d --name curso-mongo  -p 27017:27017 mongo:4.2.6
```

### 1.2.3 Crear un Volumen para no perder los datos de la Base de datos
```
docker volume create mongodb-curso
```

Ahora con el Volumen podemos crear el contenedor con la asociacion al volumen

Para el caso de una Base de datos Mongo, la informacion se gurdar en **data/db**
De la siguiente manera hacemos la vinculacion

-v **mongodb-curso:/data/db**

```
docker run -d --name curso-mongo  
-p 27017:27017 
-v mongodb-curso:/data/db 
mongo:4.2.6
```

# 1.3 Dockerfile

## 1.3.1 Buscar la mejor imagen  para Java 
```
FROM openjdk:12.0.2
FROM openjdk:11-slim
```

## 1.3.2 Dockerfile
```
FROM openjdk:11-slim
LABEL maintainer="hmunoz@unrn.edu.ar"
LABEL "unrn.edu.ar"="UNRN"
LABEL "service"="API Rest"
COPY ./build/libs/*.jar app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=docker","-jar","app.jar"]
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]
EXPOSE 8080
```
[Dockerfile Java](https://gitlab.com/public-unrn/curso-docker/snippets/1991898)



### 1.3.2.1 Crear Imagen de Docker
```
docker build -t backend:1.0 .
```

## 1.3.3 Dockerfile Multi Stage 
```
FROM openjdk:11-slim AS builder
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME
COPY . .
RUN ./gradlew bootjar

FROM openjdk:11-slim AS docker
LABEL maintainer="hmunoz@unrn.edu.ar"
LABEL "unrn.edu.ar"="UNRN"
LABEL "service"="API Rest"
COPY --from=builder /usr/app/build/libs/*.jar app.jar
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=docker","-jar","app.jar"]
ENTRYPOINT ["java", "-Djava.security.egd=file:/dev/./urandom","-jar","app.jar"]
EXPOSE 8080
```

### 1.3.3.1 Crear Imagen de Docker
```
docker build -t backend:1.0 . -f DockerfileMS
```

## 1.3.4 Registry Local -  build y push

### 1.3.5 Definir Registro Local de servcio de Imagenes
```
docker run -d -p 5000:5000 --restart=always --name registry registry:2
```

## 1.3.5.1 Registry Local -  build y push
```
docker build -t 127.0.0.1:5000/backend:1.0 .
docker push 127.0.0.1:5000/backend:1.0
```

## 1.3.6 Crear contenedor de Docker y que corra en el puerto 9010

**ERROR:**  Connection refused (Connection refused) Mongo

```
docker run --name curso-backend -p 9010:8080 backend:1.0
```

### 1.3.6.1 Agregar link --link curso-mongo:mongodb
```
docker run --name curso-backend -p 9010:8080 --link curso-mongo:mongodb backend:1.0
```

## 1.3.7 Agregar variable de entorno en Docker run

En el coso que el dockerfile no definimos el perfil (dev, prod, docker), se lo podemos pasar por parametro

**-e "SPRING_PROFILES_ACTIVE=docker"**
```
docker run --name curso-backend -p 9010:8080 --link curso-mongo:mongodb -e "SPRING_PROFILES_ACTIVE=docker" backend:1.0
```


# 1.4 Docker Compose

![Screenshot](compose.png)

```
version: '3.3'
services:
  curso-mongo:
    image: mongo:4.2.6
    container_name: curso-mongo
    volumes:
      - 'mongodb-curso:/data/db'
    ports:
      - '27017:27017'
  curso-backend:
    image: backend:1.0
    build: .
    container_name: curso-backend
    environment:
      - SPRING_PROFILES_ACTIVE=docker
    ports:
      - '9010:8080'
    depends_on:
      - curso-mongo
    
volumes:
  mongodb-curso:
```


## 1.4.1 Docker-compose
```
docker-compose  up
docker-compose  up -d
docker-compose  down
docker-compose  -f docker-compose.yml up
```

## 1.4.2 Comandos de Docker compose

```
docker-compose ps
docker-compose logs -f
```

```
         Name                        Command               State            Ports          
-------------------------------------------------------------------------------------------
backend_curso-backend_1   java -Djava.security.egd=f ...   Up      0.0.0.0:9010->8080/tcp  
backend_curso-mongo_1     docker-entrypoint.sh mongod      Up      0.0.0.0:27017->27017/tcp
```

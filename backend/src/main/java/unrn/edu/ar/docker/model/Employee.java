package unrn.edu.ar.docker.model;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Field;

public class Employee {

	@Id
	private String id;

	@Field(name = "departmentId")
	private String departmentId;

	@Field(name = "name")
	private String name;

	@Field(name = "age")
	private int age;

	@Field(name = "position")
	private String position;

	public Employee() {

	}

	public Employee(String departmentId, String name, int age, String position) {
		this.departmentId = departmentId;
		this.name = name;
		this.age = age;
		this.position = position;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDepartmentId() {
		return departmentId;
	}

	public void setDepartmentId(String departmentId) {
		this.departmentId = departmentId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getPosition() {
		return position;
	}

	public void setPosition(String position) {
		this.position = position;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", departmentId=" + departmentId
				+ ", name=" + name + ", position=" + position + "]";
	}
}
